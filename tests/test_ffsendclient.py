import unittest
import os
from ffsendclient import SendClient

SEND_SERVER = 'send.volkis.com.au'


class TestUpload(unittest.TestCase):
    def test_upload_data(self):
        send = SendClient(SEND_SERVER)
        data = os.urandom(1048576)  # 1MB
        filename = 'test_upload_data.bin'
        fid, owner_token, url = send.upload(filename, data)
        self.assertTrue(fid)
        self.assertTrue(owner_token)
        self.assertTrue(url)


class TestUploadBig(unittest.TestCase):
    def test_upload_big_data(self):
        send = SendClient(SEND_SERVER)
        data = os.urandom(1024*1024*100)  # 100MB
        filename = 'test_upload_big_data.bin'
        fid, owner_token, url = send.upload(filename, data, time_limit=30)
        self.assertTrue(fid)
        self.assertTrue(owner_token)
        self.assertTrue(url)


class TestDownload(unittest.TestCase):
    def test_download_data(self):
        send = SendClient(SEND_SERVER)
        data = os.urandom(1048576)  # 1MB
        filename = 'test_download_data.bin'
        fid, owner_token, url = send.upload(filename, data)
        secret = url.split('#')[1]
        filename_down, data_down = send.download(fid, secret)
        self.assertEqual(filename, filename_down)
        self.assertEqual(data, data_down)


if __name__ == '__main__':
    unittest.main()
